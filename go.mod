module gitlab.com/euphario/proxy

go 1.16

require (
	github.com/go-httpproxy/httpproxy v0.0.0-20180417134941-6977c68bf38e
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/lib/pq v1.10.2
	github.com/onsi/gomega v1.15.0 // indirect
)
