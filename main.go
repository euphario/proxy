/*
go-httpproxy-demo is an example for HTTP and HTTPS web proxy.

Connect through HTTP proxy to HTTP:
curl -x "http://test:1234@localhost:8080" http://httpbin.org/get?a=b

Connect through HTTP proxy to HTTPS with MITM:
curl --insecure -x "http://test:1234@localhost:8080" https://httpbin.org/get?a=b

Connect through HTTPS proxy to HTTP:
curl --proxy-insecure -x "https://test:1234@localhost:8443" http://httpbin.org/get?a=b

Connect through HTTPS proxy to HTTPS with MITM:
curl --proxy-insecure --insecure -x "https://test:1234@localhost:8443" https://httpbin.org/get?a=b

*/
package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	postgres "gitlab.com/euphario/proxy/postgres"

	"github.com/go-httpproxy/httpproxy"
)

// Env struct containing pointers to external resources
type Env struct {
	db *postgres.DB
}

var logErr = log.New(os.Stderr, "ERR: ", log.LstdFlags)

func onError(ctx *httpproxy.Context, where string,
	err *httpproxy.Error, opErr error) {
	// Log errors.
	logErr.Printf("%s: %s [%s]", where, err, opErr)
}

func onAccept(ctx *httpproxy.Context, w http.ResponseWriter,
	r *http.Request) bool {
	// Handle local request has path "/info"
	if r.Method == "GET" && !r.URL.IsAbs() && r.URL.Path == "/ca.crt" {
		cf, e := ioutil.ReadFile("ca.crt")
		if e != nil {
			w.Write([]byte("Unable to find certificate: " + e.Error()))
		} else {
			w.Header().Set("Content-Type", "text/plain")
			w.Write(cf)
		}
		return true
	}
	return false
}

// func onAuth(ctx *httpproxy.Context, authType string, user string, pass string) bool {
// 	// Auth test user.
// 	if user == "test" && pass == "1234" {
// 		return true
// 	}
// 	return false
// }

func (env *Env) onConnect(ctx *httpproxy.Context, host string) (
	ConnectAction httpproxy.ConnectAction, newHost string) {
	// Apply "Man in the Middle" to all ssl connections. Never change host.

	// cat := categoryGet(env, host)
	// ctx.UserData = cat

	r := postgres.Request{
		Timestamp: time.Now(),
		Source:    getIP(ctx.Req),
		Query:     ctx.Req.Method + " " + ctx.Req.URL.Host,
	}

	if strings.Contains(ctx.Req.URL.Host, "porn") || strings.Contains(ctx.Req.URL.Host, "omegle") {
		env.db.Insert(&r)
		return httpproxy.ConnectMitm, host
	}
	return httpproxy.ConnectProxy, host
}

func (env *Env) onRequest(ctx *httpproxy.Context, req *http.Request) (
	resp *http.Response) {

	// cat := []string{}
	// if ctx.UserData != nil {
	// 	cat = ctx.UserData.([]string)
	// }

	r := postgres.Request{
		Timestamp: time.Now(),
		Source:    getIP(req),
		// Category:  cat,
		Query:   req.Method + " " + req.URL.String(),
		Content: "",
	}

	if req.Method == "POST" {
		body, err := ioutil.ReadAll(req.Body)

		if err == nil {
			r.Content = string(body)
			req.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		}
	}

	env.db.Insert(&r)
	return
}

func onResponse(ctx *httpproxy.Context, req *http.Request,
	resp *http.Response) {
	// Add header "Via: go-httpproxy".
	//resp.Header.Add("Via", "go-httpproxy")
}

func main() {
	log.SetOutput(os.Stdout)
	log.Print("Started")

	db, err := postgres.NewDB("postgres://postgres:N6ZM4piVtRtrKZZUN42u@localhost/proxy?sslmode=disable")
	if err != nil {
		log.Panic(err)
	}

	// c, err := cache.NewCache(&cache.Options{
	// 	Addr: "localhost:6379",
	// })
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// w, _ := webshrinker.NewClient(&webshrinker.Options{
	// 	Username: "bRPb95thKzQOAuybNQVE",
	// 	Password: "yCkFOSDe259SqqrUz9M5",
	// })

	env := Env{
		db: db,
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	cf, e := ioutil.ReadFile("ca.crt")
	if e != nil {
		log.Println("Certificate:", e.Error())
		os.Exit(1)
	}

	kf, e := ioutil.ReadFile("ca.key")
	if e != nil {
		log.Println("Key:", e.Error())
		os.Exit(1)
	}

	// Create a new proxy with default certificate pair.
	prx, _ := httpproxy.NewProxyCert(cf, kf)

	// Set proxy handlers.
	prx.OnError = onError
	prx.OnAccept = onAccept
	prx.OnAuth = nil
	prx.OnConnect = env.onConnect
	prx.OnRequest = env.onRequest
	prx.OnResponse = onResponse
	//prx.MitmChunked = false

	server := &http.Server{
		Addr:         ":8080",
		Handler:      prx,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
	}
	listenErrChan := make(chan error)
	go func() {
		listenErrChan <- server.ListenAndServe()
	}()
	log.Printf("Listening HTTP %s", server.Addr)

	cert, _ := tls.X509KeyPair(httpproxy.DefaultCaCert, httpproxy.DefaultCaKey)
	serverHTTPS := &http.Server{
		Addr:         ":8443",
		Handler:      prx,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
		TLSConfig: &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			},
			Certificates: []tls.Certificate{cert},
		},
	}
	listenHTTPSErrChan := make(chan error)
	go func() {
		listenHTTPSErrChan <- serverHTTPS.ListenAndServeTLS("", "")
	}()
	log.Printf("Listening HTTPS %s", serverHTTPS.Addr)

mainloop:
	for {
		select {
		case <-sigChan:
			break mainloop
		case listenErr := <-listenErrChan:
			if listenErr != nil && listenErr == http.ErrServerClosed {
				break mainloop
			}
			log.Fatal(listenErr)
		case listenErr := <-listenHTTPSErrChan:
			if listenErr != nil && listenErr == http.ErrServerClosed {
				break mainloop
			}
			log.Fatal(listenErr)
		}
	}

	shutdown := func(srv *http.Server, wg *sync.WaitGroup) {
		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
		defer cancel()
		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err == context.DeadlineExceeded {
			log.Printf("Force shutdown %s", srv.Addr)
		} else {
			log.Printf("Graceful shutdown %s", srv.Addr)
		}
		wg.Done()
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go shutdown(server, wg)
	wg.Add(1)
	go shutdown(serverHTTPS, wg)
	wg.Wait()

	log.Println("Finished")
}

// func categoryGet(env *Env, host string) []string {
// 	h := env.category.GetTLD(host, 2)

// 	r, _ := env.cache.GetCache(h)

// 	if r == "" {
// 		rr, err := env.category.Get(host)

// 		if err == nil {
// 			env.cache.SetCache(h, rr, time.Hour*24*7*30)
// 			r = rr
// 		}
// 	}

// 	if r != "" {
// 		categories, err := env.category.Parse(&r)

// 		if err != nil {
// 			log.Fatalf("Unable to parse response: %v", err)
// 			return nil
// 		}
// 		return categories
// 	}
// 	return nil
// }

// GetIP gets a requests IP address by reading off the forwarded-for
// header (for proxies) and falls back to use the remote address.
func getIP(r *http.Request) string {
	f := r.Header.Get("X-Forwarded-For")
	if f != "" {
		return f
	}
	return r.RemoteAddr
}
