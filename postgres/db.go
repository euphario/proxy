package postgres

import (
	"database/sql"
)

// Datastore interface
type Datastore interface {
	Insert(*Request) error
}

// DB struct
type DB struct {
	*sql.DB
}

// NewDB connect to DB and check availability
func NewDB(dataSourceName string) (*DB, error) {
	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return &DB{db}, nil
}
