package postgres

import (
	"time"

	"github.com/lib/pq"
)

// Request struct to insert data into DB
type Request struct {
	Timestamp time.Time
	Source    string
	Category  []string
	Query     string
	Content   string
}

// Insert new event
func (db *DB) Insert(r *Request) error {
	sqlStatement := `INSERT INTO requests (timestamp, source, category, query, content) VALUES ($1, $2, $3, $4, $5)`
	_, err := db.Exec(sqlStatement, r.Timestamp, r.Source, pq.Array(r.Category), r.Query, r.Content)

	return err
}
