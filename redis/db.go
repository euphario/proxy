package cache

import (
	"time"

	"github.com/go-redis/redis"
)

type Datastore interface {
	GetCache(string) (string, error)
	SetCache(string, string, time.Duration) error
}

type DB struct {
	*redis.Client
}

type Options struct {
	Addr     string
	Password string
	DB       int
}

func NewCache(opts *Options) (*DB, error) {

	db := redis.NewClient(&redis.Options{
		Addr:     opts.Addr,
		Password: opts.Password,
		DB:       opts.DB,
	})

	_, err := db.Ping().Result()
	if err != nil {
		return nil, err
	}

	return &DB{db}, nil
}
