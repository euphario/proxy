package cache

import (
	"time"

	"github.com/go-redis/redis"
)

func (db *DB) GetCache(key string) (string, error) {
	r, err := db.Get(key).Result()

	if err != nil {
		return "", err
	}
	if err == redis.Nil {
		return "", nil
	}
	return r, nil
}

func (db *DB) SetCache(key string, val string, timeout time.Duration) error {
	return db.Set(key, val, timeout).Err()
}
