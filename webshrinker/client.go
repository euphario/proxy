package webshrinker

import (
	b64 "encoding/base64"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

const endPoint string = "https://api.webshrinker.com/categories/v3/"

var netTransport = &http.Transport{
	Dial: (&net.Dialer{
		Timeout: 5 * time.Second,
	}).Dial,
	TLSHandshakeTimeout: 5 * time.Second,
}
var client = &http.Client{
	Timeout:   time.Second * 10,
	Transport: netTransport,
}

// Get query API for host
func (db *DB) Get(h string) (string, error) {

	var enc = b64.URLEncoding.EncodeToString([]byte(h))
	web, _ := http.NewRequest("GET", endPoint+enc+"?taxonomy=webshrinker", nil)
	web.SetBasicAuth(db.Username, db.Password)

	resp, err := client.Do(web)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "", err
	}

	return string(body), nil
}
