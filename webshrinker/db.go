package webshrinker

// Datastore webshrinker interface
type Datastore interface {
	Get(*string) ([]string, error)
	Parse(string) ([]string, error)
	Contains([]string, string) bool
	GetTLD(string, int) string
}

// Options store username and password to connect to webshrinker API
type Options struct {
	Username string
	Password string
}

var db Options

// DB main struct
type DB struct {
	Options
}

// NewClient returns a HTTP client to query and parse webshrinker API
func NewClient(opts *Options) (*DB, error) {
	db.Username = opts.Username
	db.Password = opts.Password

	return &DB{db}, nil
}
