package webshrinker

import (
	"encoding/json"
	"sort"
	"strings"
)

type category struct {
	Data []struct {
		Categories []struct {
			ID    string `json:"id"`
			Label string `json:"label"`
		} `json:"categories"`
		URL string `json:"url"`
	} `json:"data"`
}

// Parse process JSON and return slice of categories
func (db *DB) Parse(c *string) ([]string, error) {
	categories := []string{}

	var m category

	err := json.Unmarshal([]byte(*c), &m)
	if err != nil {
		return nil, err
	}

	// "{\"data\":[{\"categories\":[{\"id\":\"business\",\"label\":\"Business\"}],\"url\":\"trafficjunky.net\"}]}"
	for _, v := range m.Data {
		for _, u := range v.Categories {
			if u.Label != "" && !db.Contains(categories, u.Label) {
				categories = append(categories, u.Label)
			}
		}
	}
	return categories, nil
}

// Contains search in slice for a match
func (db *DB) Contains(s []string, searchterm string) bool {
	i := sort.SearchStrings(s, searchterm)
	return i < len(s) && s[i] == searchterm
}

// GetTLD return the last <max> of the domain name
// www.domain.com with max set to 2 returns domain.com
func (db *DB) GetTLD(ss string, max int) string {
	s := strings.Split(ss, ".")

	if len(s) == 1 {
		return s[0]
	}

	var result []string

	for i := len(s) - max; i < len(s); i++ {
		result = append(result, s[i])
	}

	return strings.Join(result, ".")
}
